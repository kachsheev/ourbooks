USE Kurs;
/* ������������� */
/*
INSERT INTO ���_������� VALUES 
(
	'��������_�������_�������',
	...,
	'��������_����������_�������'
);*/
/*------------------------------------------------------------------*/
/*Chips*/

INSERT INTO VendorSoC VALUES (1, 'Qualcomm',			'USA'	, '');
INSERT INTO VendorSoC VALUES (2, 'Texas Instruments',	'USA'	, '');
INSERT INTO VendorSoC VALUES (3, 'MediaTek',			'China'	, '');

/*Example				id		Name			IdVendor	Arch			Freq	RAM
INSERT INTO SoC VALUES (0,		'NoNameModel',	0,			'NoNameArch',	0.0,	0);*/

-- Qualcomm
INSERT INTO SoC VALUES (101,'MSM8974 (Snapdragon 800)',		1,	'Krait 400 (ARMv7)',	2.3,	2048);
INSERT INTO SoC VALUES (102,'APQ8064T (Snapdragon 600)',	1,	'Krait 300 (ARMv7)',	1.7,	1024);
INSERT INTO SoC VALUES (103,'MSM8960 (Snapdragon S4 Plus)',	1,	'Krait',				1.7,	1024);
INSERT INTO SoC VALUES (104,'MSM8660A (Snapdragon S4 Plus)',1,	'Krait',				1.7,	1024); -- ����� ���� ��������
INSERT INTO SoC VALUES (105,'MSM8260A (Snapdragon S4 Plus)',1,	'Krait',				1.7,	1024); -- ��� ��� �������
INSERT INTO SoC VALUES (106,'MSM8625Q (Snapdragon S4 Play)',1,	'Krait',				1.2,	0);	   -- ��� ������ ���

-- TI
INSERT INTO SoC VALUES (201,'OMAP3430',						2,	'Cortex A8 (ARMv7)',	0.6,	256); -- Nokia N900 
INSERT INTO SoC VALUES (202,'OMAP3630',						2,	'Cortex A8 (ARMv7)',	1.2,	512); -- Nokia N9
INSERT INTO SoC VALUES (203,'OMAP4430',						2,	'Cortex A9 (ARMv7)',	1.0,	1024); -- Motorola ATRIX 2
INSERT INTO SoC VALUES (204,'OMAP4460',						2,	'Cortex A9 (ARMv7)',	1.5,	1024); -- Galaxy Nexus

-- Mediatek
INSERT INTO SoC VALUES (301,'MT6515',						3,	'Cortex A9 (ARMv7)',	1.0,	256);
INSERT INTO SoC VALUES (302,'MT6573',						3,	'ARM11 (ARMv6)',		0.65,	256);
INSERT INTO SoC VALUES (303,'MT6575',						3,	'Cortex A9 (ARMv7)',	1.0,	512);
INSERT INTO SoC VALUES (304,'MT6589',						3,	'Cortex A7',			1.2,	1024); -- Xperia C
INSERT INTO SoC VALUES (305,'MT6589T',						3,	'Cortex A7',			1.5,	1024);
INSERT INTO SoC VALUES (306,'MT6592',						3,	'Cortex A7',			2.2,	2048);


/*------------------------------------------------------------------*/
/*Phones*/
INSERT INTO VendorPhone VALUES (1, 'HTC',			'Taiwan',		'');
INSERT INTO VendorPhone VALUES (2, 'Sony Mobile',	'Japan',		'');
INSERT INTO VendorPhone VALUES (3, 'Nokia',			'Finland',		'');
INSERT INTO VendorPhone VALUES (4, 'Motorola',		'USA',			'');
INSERT INTO VendorPhone VALUES (5, 'Samsung',		'South Korea',	'');
INSERT INTO VendorPhone VALUES (6, 'Fly',			'Great Britain','');

/*
Example
Id		Name				Id_SoC	Id_Vendor	Diag_Screen	Flash_Mem_GB	SD_card	Camera	Front_Camera*/

-- HTC
		-- Qualcomm
INSERT INTO Phone VALUES
(101, 	'One',				102,	1,			4.7,		32,				'No',	4,		'Yes')
INSERT INTO Phone VALUES
(102, 	'Windows Phone 8X',	104,	1,			4.3,		16,				'No',	8,		'Yes')
INSERT INTO Phone VALUES
(103, 	'One X+',			104,	1,			4.7,		64,				'No',	8,		'Yes')
INSERT INTO Phone VALUES
(104, 	'One XL',			104,	1,			4.7,		16,				'No',	8,		'Yes')



-- Sony Mobile
		-- Qualcomm
INSERT INTO Phone VALUES
(201, 	'Xperia Z Ultra',	101,	2,			6.44,		16,				'Yes',	8,		'Yes')
INSERT INTO Phone VALUES
(202, 	'Xperia Z1',		101,	2,			5.0,		16,				'Yes',	21.1,	'Yes')
INSERT INTO Phone VALUES
(203, 	'Xperia V',			103,	2,			4.3,		8,				'Yes',	13.0,	'Yes')
INSERT INTO Phone VALUES
(204, 	'Xperia TX',		105,	2,			4.55,		16,				'Yes',	13.0,	'Yes')

		-- Mediatek
INSERT INTO Phone VALUES
(205, 	'Xperia C',			304,	2,			5.0,		4,				'Yes',	8.0,	'Yes')

-- Nokia
		-- Qualcomm
INSERT INTO Phone VALUES
(301, 	'Lumia 1520',		101,	3,			6.0,		32,				'Yes',	20.0,		'Yes')
INSERT INTO Phone VALUES
(302, 	'Lumia 920',		103,	3,			4.5,		32,				'No',	8.7,		'Yes')
INSERT INTO Phone VALUES
(303, 	'Lumia 820',		103,	3,			4.3,		8,				'Yes',	8.7,		'Yes')

		-- TI OMAP
INSERT INTO Phone VALUES
(305, 	'N9 64gb',			302,	3,			3.9,		64,				'No',	8.0,		'Yes')
INSERT INTO Phone VALUES
(306, 	'N900',				301,	3,			3.5,		32,				'Yes',	5.0,		'Yes')

-- Motorola
		-- Qualcomm
INSERT INTO Phone VALUES
(401, 	'Droid RAZR',		103,	4,			4.3,		16,				'Yes',	8.0,		'Yes')
		-- TI OMAP
INSERT INTO Phone VALUES
(402, 	'Milestone',		201,	4,			3.7,		16,				'Yes',	5.0,		'No')
INSERT INTO Phone VALUES
(403, 	'Defy+',			202,	4,			3.7,		1,				'Yes',	5.0,		'No')
INSERT INTO Phone VALUES
(404, 	'Milestone 2',		202,	4,			3.7,		8,				'Yes',	5.0,		'No')
INSERT INTO Phone VALUES
(405, 	'Atrix 2',			203,	4,			4.3,		8,				'Yes',	8.0,		'Yes')

-- Samsung
		-- Qualcomm
INSERT INTO Phone VALUES
(501, 	'Galaxy S4 LTE',	102,	5,			5,			16,				'Yes',	13.0,	'Yes')
INSERT INTO Phone VALUES
(502, 	'Galaxy S3',		103,	5,			4.7,		16,				'Yes',	8.0,	'Yes')

		-- TI OMAP
INSERT INTO Phone VALUES
(503, 	'Galaxy S scLCD',	202,	5,			4.0,		4,				'Yes',	5.0,	'Yes')
INSERT INTO Phone VALUES
(504, 	'Galaxy Nexus',		204,	5,			4.65,		16,				'No',	5.0,	'Yes')


-- Fly
		-- Mediatek
INSERT INTO Phone VALUES
(601, 	'IQ245+ Wizard Plus',301,	6,			3.5,		0,				'Yes',	3.0,	'No')
INSERT INTO Phone VALUES
(602, 	'IQ245 Wizard',		302,	6,			3.5,		0,				'Yes',	3.0,	'No')
INSERT INTO Phone VALUES
(603, 	'IQ237 Dynamic',	303,	6,			3.5,		0,				'Yes',	3.2,	'Yes')
INSERT INTO Phone VALUES
(604, 	'IQ451 Vista',		304,	6,			5,			4,				'Yes',	8.0,	'Yes')
INSERT INTO Phone VALUES
(605, 	'IQ457 Quad',		305,	6,			5.7,		16,				'Yes',	13.0,	'Yes')

/*------------------------------------------------------------------*/
-- Consumers -- �����������
INSERT INTO Consumers VALUES
(1,'','','','Admin','???','',100)
INSERT INTO Consumers VALUES
(2,'','','','Disp','???','',10)
INSERT INTO Consumers VALUES
(3,'','','','User','???','',1)


-- Apps -- ������

-- Warehouse -- �����
INSERT INTO Warehouse VALUES(101, 0,	25000	)
INSERT INTO Warehouse VALUES(102, 5,	12000	)
INSERT INTO Warehouse VALUES(103, 8,	18000	)
INSERT INTO Warehouse VALUES(104, 2,	13000	)
INSERT INTO Warehouse VALUES(201, 6,	28000	)
INSERT INTO Warehouse VALUES(202, 10,	30000	)
INSERT INTO Warehouse VALUES(203, 9,	13000	)
INSERT INTO Warehouse VALUES(204, 9,	12000	)
INSERT INTO Warehouse VALUES(205, 8,	15000	)
INSERT INTO Warehouse VALUES(301, 1,	32000	)
INSERT INTO Warehouse VALUES(302, 6,	18000	)
INSERT INTO Warehouse VALUES(303, 8,	16000	)
INSERT INTO Warehouse VALUES(305, 7,	8900	)
INSERT INTO Warehouse VALUES(306, 5,	5600	)
INSERT INTO Warehouse VALUES(401, 8,	16500	)
INSERT INTO Warehouse VALUES(402, 4,	9900	)
INSERT INTO Warehouse VALUES(403, 8,	5800	)
INSERT INTO Warehouse VALUES(404, 2,	13900	)
INSERT INTO Warehouse VALUES(405, 1,	11000	)
INSERT INTO Warehouse VALUES(501, 0,	24000	)
INSERT INTO Warehouse VALUES(502, 4,	17800	)
INSERT INTO Warehouse VALUES(503, 9,	8700	)
INSERT INTO Warehouse VALUES(504, 0,	12500	)
INSERT INTO Warehouse VALUES(601, 0,	4550	)
INSERT INTO Warehouse VALUES(602, 9,	5600	)
INSERT INTO Warehouse VALUES(603, 7,	7700	)
INSERT INTO Warehouse VALUES(604, 9,	10900	)
INSERT INTO Warehouse VALUES(605, 0,	13450	)
