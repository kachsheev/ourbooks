USE Kurs;
/* �������� ������ */

Create Table VendorSoC
(
	Id int				NOT NULL PRIMARY KEY,
	Name char(32)		NOT NULL,
	Country char(50)	NOT NULL,
	Info text
);

Create Table SoC
(
	Id int			NOT NULL PRIMARY KEY,
	Name char(50)	NOT NULL,
	IdVendor int	NOT NULL REFERENCES VendorSoC(Id),
	Arch char(50),
	Freq float,
	RAM int
);

Create Table VendorPhone
(
	Id int				NOT NULL PRIMARY KEY,
	Name char(50)		NOT NULL,
	Country char(50)	NOT NULL,
	Info text
);

Create Table Phone
(
	Id int				NOT NULL PRIMARY KEY,
	Name char(50)		NOT NULL,
	Id_SoC int			NOT NULL REFERENCES SoC(Id),
	Id_Vendor int		NOT NULL REFERENCES VendorPhone(Id),
	DiagScreen float	NOT NULL,
	FlashMem_GB float	NOT NULL,
	SDcard char(3)		NOT NULL,	-- yes/no
	Camera float		NOT NULL,	-- Mpix
	FrontCamera char(3)	NOT NULL	-- yes/no
);

Create Table Warehouse
(
	Id_Phone int	NOT NULL PRIMARY KEY REFERENCES Phone(Id),
	Count int		NOT NULL,
	PriceForOne int	NOT NULL
);

Create Table Consumers
(
	Id int				NOT NULL PRIMARY KEY,
	Name char(64)		NOT NULL,
	LastName char(64)	NOT NULL,
	CellPhone char(64)	NOT NULL,
	Log_in char(128)	NOT NULL UNIQUE,
	Passwd char(128)	NOT NULL, -- ������������ 
	Email char(128)		NOT NULL,
	Permission int		NOT NULL
);

Create Table Apps
(
	Id int			NOT NULL,
	Id_client int	NOT NULL REFERENCES Consumers(Id),
	Id_Phone int	NOT NULL REFERENCES Phone(Id),
	Count int		NOT NULL,
	TotalPrice int	NOT NULL,
	Status int		NOT NULL
);
