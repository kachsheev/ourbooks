/********************************************************************************
** Form generated from reading UI file 'disp_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DISP_DIALOG_H
#define UI_DISP_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Disp_Dialog
{
public:
    QTableWidget *tableWidget;
    QPushButton *pushButton;
    QComboBox *comboBox;

    void setupUi(QDialog *Disp_Dialog)
    {
        if (Disp_Dialog->objectName().isEmpty())
            Disp_Dialog->setObjectName(QStringLiteral("Disp_Dialog"));
        Disp_Dialog->resize(320, 500);
        tableWidget = new QTableWidget(Disp_Dialog);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 302, 431));
        pushButton = new QPushButton(Disp_Dialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 450, 101, 31));
        comboBox = new QComboBox(Disp_Dialog);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(240, 450, 69, 31));

        retranslateUi(Disp_Dialog);

        QMetaObject::connectSlotsByName(Disp_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Disp_Dialog)
    {
        Disp_Dialog->setWindowTitle(QApplication::translate("Disp_Dialog", "Dialog", 0));
        pushButton->setText(QApplication::translate("Disp_Dialog", "See App", 0));
    } // retranslateUi

};

namespace Ui {
    class Disp_Dialog: public Ui_Disp_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DISP_DIALOG_H
