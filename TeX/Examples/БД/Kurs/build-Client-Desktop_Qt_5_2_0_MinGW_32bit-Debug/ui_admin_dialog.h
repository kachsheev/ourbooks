/********************************************************************************
** Form generated from reading UI file 'admin_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_DIALOG_H
#define UI_ADMIN_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Admin_Dialog
{
public:
    QTabWidget *tabWidget;
    QWidget *tab_Phone;
    QTableWidget *tableWidget_Phone;
    QPushButton *pushButton_addPhone;
    QPushButton *pushButton_deletePhone;
    QWidget *tab_VendorPhone;
    QTableWidget *tableWidget_VendorPhone;
    QPushButton *pushButton_addPhoneVendor;
    QPushButton *pushButton_deletePhoneVendor;
    QWidget *tab_SoC;
    QTableWidget *tableWidget_SoC;
    QPushButton *pushButton_addSoC;
    QPushButton *pushButton_deleteSoC;
    QWidget *tab_VendorSoC;
    QTableWidget *tableWidget_VendorSoC;
    QPushButton *pushButton_addSoCVendor;
    QPushButton *pushButton_deleteSoCVendor;
    QWidget *tab_Apps;
    QTableWidget *tableWidget_Apps;
    QPushButton *pushButton_addApp;
    QPushButton *pushButton_deleteApp;
    QWidget *tab_Consumers;
    QTableWidget *tableWidget_Consumers;
    QPushButton *pushButton_addConsumer;
    QPushButton *pushButton_deleteConsumer;
    QWidget *tab_Warehouse;
    QTableWidget *tableWidget_Warehouse;
    QPushButton *pushButton_addRecord;
    QPushButton *pushButton_deleteRecord;
    QPushButton *pushButton_OK;
    QPushButton *pushButton_Refresh;

    void setupUi(QDialog *Admin_Dialog)
    {
        if (Admin_Dialog->objectName().isEmpty())
            Admin_Dialog->setObjectName(QStringLiteral("Admin_Dialog"));
        Admin_Dialog->resize(590, 340);
        Admin_Dialog->setSizeGripEnabled(false);
        tabWidget = new QTabWidget(Admin_Dialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 491, 331));
        tab_Phone = new QWidget();
        tab_Phone->setObjectName(QStringLiteral("tab_Phone"));
        tableWidget_Phone = new QTableWidget(tab_Phone);
        tableWidget_Phone->setObjectName(QStringLiteral("tableWidget_Phone"));
        tableWidget_Phone->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addPhone = new QPushButton(tab_Phone);
        pushButton_addPhone->setObjectName(QStringLiteral("pushButton_addPhone"));
        pushButton_addPhone->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deletePhone = new QPushButton(tab_Phone);
        pushButton_deletePhone->setObjectName(QStringLiteral("pushButton_deletePhone"));
        pushButton_deletePhone->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_Phone, QString());
        tab_VendorPhone = new QWidget();
        tab_VendorPhone->setObjectName(QStringLiteral("tab_VendorPhone"));
        tableWidget_VendorPhone = new QTableWidget(tab_VendorPhone);
        tableWidget_VendorPhone->setObjectName(QStringLiteral("tableWidget_VendorPhone"));
        tableWidget_VendorPhone->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addPhoneVendor = new QPushButton(tab_VendorPhone);
        pushButton_addPhoneVendor->setObjectName(QStringLiteral("pushButton_addPhoneVendor"));
        pushButton_addPhoneVendor->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deletePhoneVendor = new QPushButton(tab_VendorPhone);
        pushButton_deletePhoneVendor->setObjectName(QStringLiteral("pushButton_deletePhoneVendor"));
        pushButton_deletePhoneVendor->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_VendorPhone, QString());
        tab_SoC = new QWidget();
        tab_SoC->setObjectName(QStringLiteral("tab_SoC"));
        tableWidget_SoC = new QTableWidget(tab_SoC);
        tableWidget_SoC->setObjectName(QStringLiteral("tableWidget_SoC"));
        tableWidget_SoC->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addSoC = new QPushButton(tab_SoC);
        pushButton_addSoC->setObjectName(QStringLiteral("pushButton_addSoC"));
        pushButton_addSoC->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deleteSoC = new QPushButton(tab_SoC);
        pushButton_deleteSoC->setObjectName(QStringLiteral("pushButton_deleteSoC"));
        pushButton_deleteSoC->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_SoC, QString());
        tab_VendorSoC = new QWidget();
        tab_VendorSoC->setObjectName(QStringLiteral("tab_VendorSoC"));
        tableWidget_VendorSoC = new QTableWidget(tab_VendorSoC);
        tableWidget_VendorSoC->setObjectName(QStringLiteral("tableWidget_VendorSoC"));
        tableWidget_VendorSoC->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addSoCVendor = new QPushButton(tab_VendorSoC);
        pushButton_addSoCVendor->setObjectName(QStringLiteral("pushButton_addSoCVendor"));
        pushButton_addSoCVendor->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deleteSoCVendor = new QPushButton(tab_VendorSoC);
        pushButton_deleteSoCVendor->setObjectName(QStringLiteral("pushButton_deleteSoCVendor"));
        pushButton_deleteSoCVendor->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_VendorSoC, QString());
        tab_Apps = new QWidget();
        tab_Apps->setObjectName(QStringLiteral("tab_Apps"));
        tableWidget_Apps = new QTableWidget(tab_Apps);
        tableWidget_Apps->setObjectName(QStringLiteral("tableWidget_Apps"));
        tableWidget_Apps->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addApp = new QPushButton(tab_Apps);
        pushButton_addApp->setObjectName(QStringLiteral("pushButton_addApp"));
        pushButton_addApp->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deleteApp = new QPushButton(tab_Apps);
        pushButton_deleteApp->setObjectName(QStringLiteral("pushButton_deleteApp"));
        pushButton_deleteApp->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_Apps, QString());
        tab_Consumers = new QWidget();
        tab_Consumers->setObjectName(QStringLiteral("tab_Consumers"));
        tableWidget_Consumers = new QTableWidget(tab_Consumers);
        tableWidget_Consumers->setObjectName(QStringLiteral("tableWidget_Consumers"));
        tableWidget_Consumers->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addConsumer = new QPushButton(tab_Consumers);
        pushButton_addConsumer->setObjectName(QStringLiteral("pushButton_addConsumer"));
        pushButton_addConsumer->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deleteConsumer = new QPushButton(tab_Consumers);
        pushButton_deleteConsumer->setObjectName(QStringLiteral("pushButton_deleteConsumer"));
        pushButton_deleteConsumer->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_Consumers, QString());
        tab_Warehouse = new QWidget();
        tab_Warehouse->setObjectName(QStringLiteral("tab_Warehouse"));
        tableWidget_Warehouse = new QTableWidget(tab_Warehouse);
        tableWidget_Warehouse->setObjectName(QStringLiteral("tableWidget_Warehouse"));
        tableWidget_Warehouse->setGeometry(QRect(0, 10, 391, 291));
        pushButton_addRecord = new QPushButton(tab_Warehouse);
        pushButton_addRecord->setObjectName(QStringLiteral("pushButton_addRecord"));
        pushButton_addRecord->setGeometry(QRect(400, 10, 75, 31));
        pushButton_deleteRecord = new QPushButton(tab_Warehouse);
        pushButton_deleteRecord->setObjectName(QStringLiteral("pushButton_deleteRecord"));
        pushButton_deleteRecord->setGeometry(QRect(400, 50, 75, 31));
        tabWidget->addTab(tab_Warehouse, QString());
        pushButton_OK = new QPushButton(Admin_Dialog);
        pushButton_OK->setObjectName(QStringLiteral("pushButton_OK"));
        pushButton_OK->setGeometry(QRect(500, 30, 81, 31));
        pushButton_Refresh = new QPushButton(Admin_Dialog);
        pushButton_Refresh->setObjectName(QStringLiteral("pushButton_Refresh"));
        pushButton_Refresh->setGeometry(QRect(500, 70, 81, 31));

        retranslateUi(Admin_Dialog);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Admin_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Admin_Dialog)
    {
        Admin_Dialog->setWindowTitle(QApplication::translate("Admin_Dialog", "Administator", 0));
        pushButton_addPhone->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deletePhone->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Phone), QApplication::translate("Admin_Dialog", "Phones", 0));
        pushButton_addPhoneVendor->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deletePhoneVendor->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_VendorPhone), QApplication::translate("Admin_Dialog", "Phone Vendors", 0));
        pushButton_addSoC->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deleteSoC->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_SoC), QApplication::translate("Admin_Dialog", "SoCs", 0));
        pushButton_addSoCVendor->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deleteSoCVendor->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_VendorSoC), QApplication::translate("Admin_Dialog", "SoC Vendors", 0));
        pushButton_addApp->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deleteApp->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Apps), QApplication::translate("Admin_Dialog", "Apps", 0));
        pushButton_addConsumer->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deleteConsumer->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Consumers), QApplication::translate("Admin_Dialog", "Consumers", 0));
        pushButton_addRecord->setText(QApplication::translate("Admin_Dialog", "Add", 0));
        pushButton_deleteRecord->setText(QApplication::translate("Admin_Dialog", "Delete", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Warehouse), QApplication::translate("Admin_Dialog", "Warehouse", 0));
        pushButton_OK->setText(QApplication::translate("Admin_Dialog", "OK", 0));
        pushButton_Refresh->setText(QApplication::translate("Admin_Dialog", "Refresh", 0));
    } // retranslateUi

};

namespace Ui {
    class Admin_Dialog: public Ui_Admin_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_DIALOG_H
