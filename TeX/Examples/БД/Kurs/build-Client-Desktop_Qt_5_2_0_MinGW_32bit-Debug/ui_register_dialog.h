/********************************************************************************
** Form generated from reading UI file 'register_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTER_DIALOG_H
#define UI_REGISTER_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Register_Dialog
{
public:
    QLabel *label;
    QLabel *label_4;
    QLineEdit *lineEdit_Login;
    QLabel *label_7;
    QLineEdit *lineEdit_Name;
    QLineEdit *lineEdit_LastName;
    QLineEdit *lineEdit_CellPhone;
    QLabel *label_8;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_2;
    QLineEdit *lineEdit_Email1;
    QLineEdit *lineEdit_Email2;
    QLineEdit *lineEdit_Passwd1;
    QLineEdit *lineEdit_Passwd2;
    QPushButton *pushButton_OK;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *Register_Dialog)
    {
        if (Register_Dialog->objectName().isEmpty())
            Register_Dialog->setObjectName(QStringLiteral("Register_Dialog"));
        Register_Dialog->resize(370, 340);
        label = new QLabel(Register_Dialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 60, 91, 21));
        label_4 = new QLabel(Register_Dialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 90, 91, 21));
        lineEdit_Login = new QLineEdit(Register_Dialog);
        lineEdit_Login->setObjectName(QStringLiteral("lineEdit_Login"));
        lineEdit_Login->setGeometry(QRect(110, 20, 241, 21));
        label_7 = new QLabel(Register_Dialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 20, 91, 21));
        lineEdit_Name = new QLineEdit(Register_Dialog);
        lineEdit_Name->setObjectName(QStringLiteral("lineEdit_Name"));
        lineEdit_Name->setGeometry(QRect(110, 60, 241, 21));
        lineEdit_LastName = new QLineEdit(Register_Dialog);
        lineEdit_LastName->setObjectName(QStringLiteral("lineEdit_LastName"));
        lineEdit_LastName->setGeometry(QRect(110, 90, 241, 21));
        lineEdit_CellPhone = new QLineEdit(Register_Dialog);
        lineEdit_CellPhone->setObjectName(QStringLiteral("lineEdit_CellPhone"));
        lineEdit_CellPhone->setGeometry(QRect(110, 120, 241, 21));
        label_8 = new QLabel(Register_Dialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 120, 91, 21));
        label_3 = new QLabel(Register_Dialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 160, 91, 21));
        label_5 = new QLabel(Register_Dialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 260, 91, 21));
        label_6 = new QLabel(Register_Dialog);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 190, 91, 21));
        label_2 = new QLabel(Register_Dialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 230, 91, 21));
        lineEdit_Email1 = new QLineEdit(Register_Dialog);
        lineEdit_Email1->setObjectName(QStringLiteral("lineEdit_Email1"));
        lineEdit_Email1->setGeometry(QRect(110, 160, 241, 21));
        lineEdit_Email2 = new QLineEdit(Register_Dialog);
        lineEdit_Email2->setObjectName(QStringLiteral("lineEdit_Email2"));
        lineEdit_Email2->setGeometry(QRect(110, 190, 241, 21));
        lineEdit_Passwd1 = new QLineEdit(Register_Dialog);
        lineEdit_Passwd1->setObjectName(QStringLiteral("lineEdit_Passwd1"));
        lineEdit_Passwd1->setGeometry(QRect(110, 230, 241, 21));
        lineEdit_Passwd2 = new QLineEdit(Register_Dialog);
        lineEdit_Passwd2->setObjectName(QStringLiteral("lineEdit_Passwd2"));
        lineEdit_Passwd2->setGeometry(QRect(110, 260, 241, 21));
        pushButton_OK = new QPushButton(Register_Dialog);
        pushButton_OK->setObjectName(QStringLiteral("pushButton_OK"));
        pushButton_OK->setGeometry(QRect(70, 300, 75, 31));
        pushButton_Cancel = new QPushButton(Register_Dialog);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setGeometry(QRect(230, 300, 75, 31));

        retranslateUi(Register_Dialog);

        QMetaObject::connectSlotsByName(Register_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Register_Dialog)
    {
        Register_Dialog->setWindowTitle(QApplication::translate("Register_Dialog", "Dialog", 0));
        label->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Name</p></body></html>", 0));
        label_4->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Last Name</p></body></html>", 0));
        label_7->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Your Login*</p></body></html>", 0));
        label_8->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Cell Phone</p></body></html>", 0));
        label_3->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Email*</p></body></html>", 0));
        label_5->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Correct Password*</p></body></html>", 0));
        label_6->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Correct Email*</p></body></html>", 0));
        label_2->setText(QApplication::translate("Register_Dialog", "<html><head/><body><p align=\"right\">Password*</p></body></html>", 0));
        pushButton_OK->setText(QApplication::translate("Register_Dialog", "OK", 0));
        pushButton_Cancel->setText(QApplication::translate("Register_Dialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class Register_Dialog: public Ui_Register_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTER_DIALOG_H
