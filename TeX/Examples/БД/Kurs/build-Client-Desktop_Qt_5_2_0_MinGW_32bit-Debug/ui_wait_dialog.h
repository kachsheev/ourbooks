/********************************************************************************
** Form generated from reading UI file 'wait_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WAIT_DIALOG_H
#define UI_WAIT_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_Wait_Dialog
{
public:
    QLabel *label;

    void setupUi(QDialog *Wait_Dialog)
    {
        if (Wait_Dialog->objectName().isEmpty())
            Wait_Dialog->setObjectName(QStringLiteral("Wait_Dialog"));
        Wait_Dialog->resize(140, 60);
        label = new QLabel(Wait_Dialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(5, 10, 131, 41));

        retranslateUi(Wait_Dialog);

        QMetaObject::connectSlotsByName(Wait_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Wait_Dialog)
    {
        Wait_Dialog->setWindowTitle(QApplication::translate("Wait_Dialog", "Dialog", 0));
        label->setText(QApplication::translate("Wait_Dialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt;\">Please, wait...</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class Wait_Dialog: public Ui_Wait_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WAIT_DIALOG_H
