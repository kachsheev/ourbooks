/********************************************************************************
** Form generated from reading UI file 'consumer_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONSUMER_DIALOG_H
#define UI_CONSUMER_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Consumer_Dialog
{
public:
    QTableWidget *tableWidget_Phones;
    QGroupBox *groupBox;
    QLabel *label_2;
    QLabel *label;
    QComboBox *comboBox_VendorPhone;
    QComboBox *comboBox_VendorSoC;
    QGroupBox *groupBox_2;
    QComboBox *comboBox_Freq1;
    QComboBox *comboBox_Freq2;
    QComboBox *comboBox_Diag1;
    QComboBox *comboBox_Diag2;
    QComboBox *comboBox_RAM;
    QComboBox *comboBox_FlashMem;
    QComboBox *comboBox_SDcard;
    QComboBox *comboBox_FrontCam;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_11;
    QComboBox *comboBox_BackCam;
    QComboBox *comboBox_Status;
    QLabel *label_12;
    QGroupBox *groupBox_3;
    QLabel *label_10;
    QComboBox *comboBox_ChooseModel;
    QLabel *label_3;
    QComboBox *comboBox_countModels;
    QPushButton *pushButton_Cancel;
    QPushButton *pushButton_Order;

    void setupUi(QDialog *Consumer_Dialog)
    {
        if (Consumer_Dialog->objectName().isEmpty())
            Consumer_Dialog->setObjectName(QStringLiteral("Consumer_Dialog"));
        Consumer_Dialog->setEnabled(true);
        Consumer_Dialog->resize(800, 520);
        Consumer_Dialog->setToolTipDuration(4);
        tableWidget_Phones = new QTableWidget(Consumer_Dialog);
        tableWidget_Phones->setObjectName(QStringLiteral("tableWidget_Phones"));
        tableWidget_Phones->setGeometry(QRect(315, 10, 481, 501));
        groupBox = new QGroupBox(Consumer_Dialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 301, 91));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 50, 81, 21));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 20, 91, 21));
        comboBox_VendorPhone = new QComboBox(groupBox);
        comboBox_VendorPhone->setObjectName(QStringLiteral("comboBox_VendorPhone"));
        comboBox_VendorPhone->setGeometry(QRect(140, 20, 151, 22));
        comboBox_VendorSoC = new QComboBox(groupBox);
        comboBox_VendorSoC->setObjectName(QStringLiteral("comboBox_VendorSoC"));
        comboBox_VendorSoC->setGeometry(QRect(140, 50, 151, 22));
        groupBox_2 = new QGroupBox(Consumer_Dialog);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 110, 301, 231));
        comboBox_Freq1 = new QComboBox(groupBox_2);
        comboBox_Freq1->setObjectName(QStringLiteral("comboBox_Freq1"));
        comboBox_Freq1->setGeometry(QRect(140, 20, 69, 22));
        comboBox_Freq2 = new QComboBox(groupBox_2);
        comboBox_Freq2->setObjectName(QStringLiteral("comboBox_Freq2"));
        comboBox_Freq2->setGeometry(QRect(220, 20, 69, 22));
        comboBox_Diag1 = new QComboBox(groupBox_2);
        comboBox_Diag1->setObjectName(QStringLiteral("comboBox_Diag1"));
        comboBox_Diag1->setGeometry(QRect(140, 50, 69, 22));
        comboBox_Diag2 = new QComboBox(groupBox_2);
        comboBox_Diag2->setObjectName(QStringLiteral("comboBox_Diag2"));
        comboBox_Diag2->setGeometry(QRect(220, 50, 69, 22));
        comboBox_RAM = new QComboBox(groupBox_2);
        comboBox_RAM->setObjectName(QStringLiteral("comboBox_RAM"));
        comboBox_RAM->setGeometry(QRect(140, 80, 151, 22));
        comboBox_FlashMem = new QComboBox(groupBox_2);
        comboBox_FlashMem->setObjectName(QStringLiteral("comboBox_FlashMem"));
        comboBox_FlashMem->setGeometry(QRect(140, 110, 151, 22));
        comboBox_SDcard = new QComboBox(groupBox_2);
        comboBox_SDcard->setObjectName(QStringLiteral("comboBox_SDcard"));
        comboBox_SDcard->setGeometry(QRect(140, 140, 151, 22));
        comboBox_FrontCam = new QComboBox(groupBox_2);
        comboBox_FrontCam->setObjectName(QStringLiteral("comboBox_FrontCam"));
        comboBox_FrontCam->setGeometry(QRect(140, 170, 151, 22));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 50, 121, 21));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 20, 121, 21));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 80, 121, 21));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 110, 121, 21));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 140, 121, 21));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 170, 121, 21));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 200, 121, 21));
        comboBox_BackCam = new QComboBox(groupBox_2);
        comboBox_BackCam->setObjectName(QStringLiteral("comboBox_BackCam"));
        comboBox_BackCam->setGeometry(QRect(140, 200, 151, 22));
        comboBox_Status = new QComboBox(Consumer_Dialog);
        comboBox_Status->setObjectName(QStringLiteral("comboBox_Status"));
        comboBox_Status->setGeometry(QRect(150, 350, 69, 21));
        label_12 = new QLabel(Consumer_Dialog);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(20, 350, 121, 20));
        groupBox_3 = new QGroupBox(Consumer_Dialog);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 380, 301, 131));
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(40, 50, 91, 21));
        comboBox_ChooseModel = new QComboBox(groupBox_3);
        comboBox_ChooseModel->setObjectName(QStringLiteral("comboBox_ChooseModel"));
        comboBox_ChooseModel->setGeometry(QRect(140, 20, 151, 22));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(40, 20, 91, 21));
        comboBox_countModels = new QComboBox(groupBox_3);
        comboBox_countModels->setObjectName(QStringLiteral("comboBox_countModels"));
        comboBox_countModels->setGeometry(QRect(140, 50, 69, 21));
        pushButton_Cancel = new QPushButton(groupBox_3);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setGeometry(QRect(220, 90, 71, 31));
        pushButton_Order = new QPushButton(groupBox_3);
        pushButton_Order->setObjectName(QStringLiteral("pushButton_Order"));
        pushButton_Order->setGeometry(QRect(10, 90, 71, 31));

        retranslateUi(Consumer_Dialog);

        QMetaObject::connectSlotsByName(Consumer_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Consumer_Dialog)
    {
        Consumer_Dialog->setWindowTitle(QApplication::translate("Consumer_Dialog", "Dialog", 0));
        groupBox->setTitle(QApplication::translate("Consumer_Dialog", "Vendors", 0));
        label_2->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Vendor SoC: </p></body></html>", 0));
        label->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Vendor Phone: </p></body></html>", 0));
        groupBox_2->setTitle(QApplication::translate("Consumer_Dialog", "Characters", 0));
        label_4->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Diagonal</p></body></html>", 0));
        label_5->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Frequency</p></body></html>", 0));
        label_6->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">RAM</p></body></html>", 0));
        label_7->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Flash memory</p></body></html>", 0));
        label_8->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Slot for SD-card</p></body></html>", 0));
        label_9->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Front camera</p></body></html>", 0));
        label_11->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Back camera</p></body></html>", 0));
        comboBox_Status->clear();
        comboBox_Status->insertItems(0, QStringList()
         << QApplication::translate("Consumer_Dialog", "All", 0)
         << QApplication::translate("Consumer_Dialog", "Exist", 0)
         << QApplication::translate("Consumer_Dialog", "None", 0)
        );
        label_12->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Status</p></body></html>", 0));
        groupBox_3->setTitle(QApplication::translate("Consumer_Dialog", "Choose", 0));
        label_10->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Count: </p></body></html>", 0));
        label_3->setText(QApplication::translate("Consumer_Dialog", "<html><head/><body><p align=\"right\">Model: </p></body></html>", 0));
        pushButton_Cancel->setText(QApplication::translate("Consumer_Dialog", "Cancel", 0));
        pushButton_Order->setText(QApplication::translate("Consumer_Dialog", "Order", 0));
    } // retranslateUi

};

namespace Ui {
    class Consumer_Dialog: public Ui_Consumer_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONSUMER_DIALOG_H
