#ifndef CONSUMER_DIALOG_H
#define CONSUMER_DIALOG_H

#include <QDialog>
#include <QSqlDatabase>

namespace Ui {
class Consumer_Dialog;
}

class Consumer_Dialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit Consumer_Dialog(QWidget *parent = 0);
	~Consumer_Dialog();
	
	void setDB(QSqlDatabase *db);
	
private slots:
	
	void on_pushButton_Order_clicked();
	void on_pushButton_Cancel_clicked();
	
	void on_comboBox_VendorPhone_currentTextChanged(const QString &);
	void on_comboBox_VendorSoC_currentTextChanged(const QString &);
	void on_comboBox_Freq1_currentTextChanged(const QString &);
	void on_comboBox_Freq2_currentTextChanged(const QString &);
	void on_comboBox_Diag1_currentTextChanged(const QString &);
	void on_comboBox_Diag2_currentTextChanged(const QString &);
	void on_comboBox_RAM_currentTextChanged(const QString &);
	void on_comboBox_FlashMem_currentTextChanged(const QString &);
	void on_comboBox_SDcard_currentTextChanged(const QString &);
	void on_comboBox_FrontCam_currentTextChanged(const QString &);
	void on_comboBox_BackCam_currentTextChanged(const QString &);
	void on_comboBox_Status_currentTextChanged(const QString &);
	void on_comboBox_ChooseModel_currentTextChanged(const QString &phone);
	
	
protected:
	void resizeEvent(QResizeEvent *);	
	
private:
	Ui::Consumer_Dialog *ui;
	
	QSqlDatabase *db;
	QString userLogin;
	
	void firstUpdateView();
	void updateView();
	void setUser(QString login);
	void removeUser();
};

#endif // CONSUMER_DIALOG_H
