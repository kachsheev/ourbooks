#ifndef SEEAPP_DIALOG_H
#define SEEAPP_DIALOG_H

#include <QDialog>
#include "consumer_dialog.h"

namespace Ui {
class SeeApp_Dialog;
}

class SeeApp_Dialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit SeeApp_Dialog(QWidget *parent = 0);
	~SeeApp_Dialog();
	
	void viewInfo();
	
private slots:
	void on_pushButton_OK_clicked();
	
	void on_pushButton_Cancel_clicked();
	
private:
	Ui::SeeApp_Dialog *ui;
	
	Consumer_Dialog *parent;
	
	void changeStatus();
};

#endif // SEEAPP_DIALOG_H
