#ifndef WAIT_DIALOG_H
#define WAIT_DIALOG_H

#include <QDialog>

namespace Ui {
class Wait_Dialog;
}

class Wait_Dialog : public QDialog
{
	Q_OBJECT

public:
	explicit Wait_Dialog(QWidget *parent = 0);
	~Wait_Dialog();

private:
	Ui::Wait_Dialog *ui;
};

#endif // WAIT_DIALOG_H
