#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QtSql>
#include <QSqlDatabase>
#include <QCryptographicHash>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	
	qDebug() << "Starting";
	ui->setupUi(this);
	
	this->setFixedHeight(this->height());
	this->setFixedWidth(this->width());
	
	db = QSqlDatabase::addDatabase("QODBC");
	// for noteboot
	db.setDatabaseName("DRIVER={SQL Server};Server=ASPIRE-4830TG\\AKASHCHEEV_SQL;Database=Kurs;Trusted_Connection=yes");
//	for desktop
//	db.setDatabaseName("DRIVER={SQL Server};Server=GLITE\\AKASHCHEEV_SQL;Database=Kurs;Trusted_Connection=yes");
	db.setUserName("kachsheev");
	db.setPassword("");
	
	if (!db.open()) {
		wait->hide();
		qDebug() << "Can not connect to database:" << db.lastError();
		return;
	}

	admin = new Admin_Dialog(this);
	qDebug() << "Admin";
	regist = new Register_Dialog(this);
	qDebug() << "Regist";
	client = new Consumer_Dialog(this);
	qDebug() << "Client";
	disp = new Disp_Dialog(this);
	qDebug() << "Disp";
	
	admin->setDB(&db);	
	regist->setDB(&db);
	client->setDB(&db);
	disp->setDB(&db);
	
	ui->lineEdit_Passwd->setEchoMode(QLineEdit::Password);
	
	qDebug() << "Stoping";
//	delete wait;
	
}

void MainWindow::on_pushButton_Exit_clicked()
{
	qApp->quit();
}

void MainWindow::on_pushButton_Connect_clicked()
{
			QSqlQuery query(db);
			
			QString login = ui->lineEdit_Login->text(),
					passwd = ui->lineEdit_Passwd->text(),
					str = "SELECT COUNT(*) FROM Consumers WHERE ('";
			
//			QByteArray login_hash = QCryptographicHash::hash(login.toUtf8(),
//									QCryptographicHash::Md5); login = "";
			QByteArray passwd_hash = QCryptographicHash::hash(passwd.toUtf8(),
									  QCryptographicHash::Md5); passwd = "";
			
//			login.append(login_hash);
			passwd.append(passwd_hash);
			
			str+= login + "'=Log_in AND '" + passwd + "'=Passwd)";
			
			qDebug() << str;
			
			if(query.exec(str))
			{
				query.next();
				if(1==query.value(0).toInt())
				{
					str = "SELECT Consumers.Permission FROM Consumers WHERE";
					str +="('" + ui->lineEdit_Login->text() + "'=Log_in)";
					
					qDebug() << str;
					
					if(!query.exec(str))
					{
						qDebug() << "Bad query";
					}
					else
					{
						query.next();
						switch(query.value(0).toInt())
						{
						case 100:
							admin->show();
							break;
						case 10:
							disp->show();
							break;
						case 1:
							client->show();
							client->setWindowTitle(
										ui->lineEdit_Login->text());
							break;
						}
					}
										
					ui->lineEdit_Login->setText("");
					ui->lineEdit_Passwd->setText("");
				}
			}
			else
			{
				qDebug() << "Bad query";
			}
			
}

void MainWindow::on_pushButton_Register_clicked()
{
//	this->hide();
	regist->show();
}

MainWindow::~MainWindow()
{
	delete admin;
	delete regist;
	delete client;
	
	delete ui;
}
