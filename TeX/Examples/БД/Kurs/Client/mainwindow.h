#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QResizeEvent>

#include "wait_dialog.h"
#include "admin_dialog.h"
#include "register_dialog.h"
#include "consumer_dialog.h"
#include "disp_dialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	
	QApplication *app;
	
	QSqlDatabase db;
	
	Wait_Dialog *wait;
	Admin_Dialog *admin;
	Consumer_Dialog *client;
	Disp_Dialog *disp;
	
	Register_Dialog *regist;
	
	
private slots:
	void on_pushButton_Exit_clicked();
	
	void on_pushButton_Connect_clicked();
	
	void on_pushButton_Register_clicked();
	
private:
	Ui::MainWindow *ui;
	
};

#endif // MAINWINDOW_H
