#include "admin_dialog.h"
#include "ui_admin_dialog.h"

#include <mainwindow.h>
#include <iostream>


Admin_Dialog::Admin_Dialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Admin_Dialog)
{
	ui->setupUi(this);
	this->parent = parent;
}

void Admin_Dialog::resizeEvent(QResizeEvent *)
{
	ui->tabWidget->resize(this->width()-99, this->height()-9);
	ui->tableWidget_Phone->resize(		this->width()-199, this->height()-49);
	ui->tableWidget_VendorPhone->resize(this->width()-199, this->height()-49);
	ui->tableWidget_SoC->resize(		this->width()-199, this->height()-49);
	ui->tableWidget_VendorSoC->resize(	this->width()-199, this->height()-49);
	ui->tableWidget_Apps->resize(		this->width()-199, this->height()-49);
	ui->tableWidget_Warehouse->resize(	this->width()-199, this->height()-49);
	ui->tableWidget_Consumers->resize(	this->width()-199, this->height()-49);
	ui->pushButton_OK->move(			this->width()-90, 30);
	ui->pushButton_Refresh->move(		this->width()-90, 70);
}

void Admin_Dialog::setDB(QSqlDatabase *db)
{
	this->db = db;
}

Admin_Dialog::~Admin_Dialog()
{
	delete ui;
}

void Admin_Dialog::on_pushButton_OK_clicked()
{
    this->hide();
	this->parent->show();
}

void Admin_Dialog::on_pushButton_Refresh_clicked()
{
	this->refresh_Phone();
	this->refresh_VendorPhone();
	this->refresh_SoC();
	this->refresh_VendorSoC();
	this->refresh_Apps();
	this->refresh_Consumers();
	this->refresh_Warehouse();
}

void Admin_Dialog::refresh_Phone()
{
	QSqlQuery query(*db);
	
	//Phone
	QString str = "SELECT * FROM Phone";
	QStringList strlist;
	strlist << "Id" << "Name" << "Id_SoC" << "Id_Vendor" << "Diag Screen" << "FlashMem_GB"
			<< "SDcard" << "Camera" << "FrontCamera";
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh Phone";
	}
	else
	{
		ui->tableWidget_Phone->setColumnCount(query.record().count());
		ui->tableWidget_Phone->setHorizontalHeaderLabels(strlist);
		int i = 0;
		while(query.next())
		{
			ui->tableWidget_Phone->setRowCount(i+1);
			for(int j=0; j<query.record().count(); j++)
			{
				ui->tableWidget_Phone->setItem(i,j,
						new QTableWidgetItem(query.value(j).toString(),1));
			}
			i++;
		}
	}
	
}

void Admin_Dialog::refresh_VendorPhone()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM VendorPhone";
	QStringList strlist;
	strlist << "Id" << "Name" << "Country"
			<< "Info";
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh VendorPhone";
	}
	else
	{
		ui->tableWidget_VendorPhone->setColumnCount(query.record().count());
		ui->tableWidget_VendorPhone->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_VendorPhone->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_VendorPhone->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}

void Admin_Dialog::refresh_SoC()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM SoC";
	QStringList strlist;
	strlist << "Id" << "Name" << "IdVendor"
			<< "Arch" << "Freq" << "RAM";
	
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh SoC";
	}
	else
	{
		ui->tableWidget_SoC->setColumnCount(query.record().count());
		ui->tableWidget_SoC->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_SoC->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_SoC->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}

void Admin_Dialog::refresh_VendorSoC()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM VendorSoC";
	QStringList strlist;
	strlist << "Id" << "Name" << "Country"
			<< "Info";
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh VendorSoC";
	}
	else
	{
		ui->tableWidget_VendorSoC->setColumnCount(query.record().count());
		ui->tableWidget_VendorSoC->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_VendorSoC->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_VendorSoC->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}

void Admin_Dialog::refresh_Consumers()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM Consumers";
	QStringList strlist;
	strlist << "Id" << "Name" << "Last name" << "Cell Phone" << "Login"
			<< "Passwd" << "E-mail" << "Permission";
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh Consumers";
	}
	else
	{
		ui->tableWidget_Consumers->setColumnCount(query.record().count());
		ui->tableWidget_Consumers->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_Consumers->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_Consumers->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}

void Admin_Dialog::refresh_Warehouse()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM Warehouse";
	QStringList strlist;
	strlist << "Id_Phone" << "Count" << "Price";
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh Warehouse";
	}
	else
	{
		ui->tableWidget_Warehouse->setColumnCount(query.record().count());
		ui->tableWidget_Warehouse->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_Warehouse->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_Warehouse->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}

void Admin_Dialog::refresh_Apps()
{
	QSqlQuery query(*db);
	
	//SoC
	QString str = "SELECT * FROM Apps";
	QStringList strlist;
	strlist << "Id_Client" << "Id_Phone" << "Count"
			<< "TotalPrice" << "Status";
	
	if(!query.exec(str))
	{
		qDebug() << "Can not refresh Apps";
	}
	else
	{
		ui->tableWidget_Apps->setColumnCount(query.record().count());
		ui->tableWidget_Apps->setHorizontalHeaderLabels(strlist);
		int j = 0;
		while(query.next())
		{
			ui->tableWidget_Apps->setRowCount(j+1);
			for(int i=0; i<query.record().count(); i++)
			{
				ui->tableWidget_Apps->setItem(j,i,
						new QTableWidgetItem(query.value(i).toString(),1));
			}
			j++;
		}
	}
}
