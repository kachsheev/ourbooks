#ifndef REGISTER_DIALOG_H
#define REGISTER_DIALOG_H

#include <QDialog>
#include <QSql>
#include <QSqlDatabase>

namespace Ui {
class Register_Dialog;
}

class Register_Dialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit Register_Dialog(QWidget *parent = 0);
	~Register_Dialog();
	
	void setDB(QSqlDatabase *db);
	
private slots:
	
	void on_pushButton_OK_clicked();
	
	void on_pushButton_Cancel_clicked();
	
private:
	Ui::Register_Dialog *ui;
	
	QWidget *parent;
	
	QSqlDatabase *db;
	
	int validateNewUser();
	void addNewUser();
};

#endif // REGISTER_DIALOG_H
