#ifndef ADMIN_DIALOG_H
#define ADMIN_DIALOG_H

#include <QDialog>
#include <QSql>
#include <QSqlDatabase>

namespace Ui {
class Admin_Dialog;
}

class Admin_Dialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit Admin_Dialog(QWidget *parent = 0);
	
	void setDB(QSqlDatabase *db);
	
	~Admin_Dialog();
	
private slots:
	void on_pushButton_OK_clicked();
	
	void on_pushButton_Refresh_clicked();
	
private:
	Ui::Admin_Dialog *ui;
	
	QWidget *parent;
	QSqlQuery *query;
	QSqlDatabase *db;
	
	void refresh_Phone();
	void refresh_SoC();
	void refresh_VendorPhone();
	void refresh_VendorSoC();
	void refresh_Warehouse();
	void refresh_Apps();
	void refresh_Consumers();
	
protected:
	void resizeEvent( QResizeEvent *);
};

#endif // ADMIN_DIALOG_H
