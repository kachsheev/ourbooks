#include "consumer_dialog.h"
#include "ui_consumer_dialog.h"

#include <QSql>
#include <QDebug>
#include <QSqlRecord>
#include <QSqlQuery>

Consumer_Dialog::Consumer_Dialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Consumer_Dialog)
{
	ui->setupUi(this);
	this->setFixedHeight(this->height());
//	this->setFixedWidth(this->width());
	
	QStringList str_list; str_list << "Vendor Phone" << "Model Phone" << "Diagonal"
								   << "Flash Memory" << "SD-card" << "Back Camera"
								   << "Front Camera" << "Vendor SoC" << "Model SoC" << "Frequency"
								   << "Price" << "Count";
	ui->tableWidget_Phones->setColumnCount(str_list.size());
	ui->tableWidget_Phones->setHorizontalHeaderLabels(str_list);
	
	ui->comboBox_ChooseModel->addItem("No chosen");
	ui->comboBox_countModels->addItem("No chosen");
	
	firstUpdateView();
}

Consumer_Dialog::~Consumer_Dialog()
{
	delete ui;
}

void Consumer_Dialog::on_pushButton_Order_clicked()
{
	if(ui->comboBox_countModels->currentText() != "No chosen"
			&& ui->comboBox_ChooseModel->currentText() != "No chosen")
	{
		QSqlQuery query(*(this->db));
		
		int Id_user = 0,
		Id_phone = 0,
		Id_app = 1,
		Price = 0;
		QString str;
		
		userLogin = this->windowTitle();
		
		if(!query.exec("select Consumers.Id From Consumers where (Consumers.Log_in = '"
					   + userLogin +"')"))
		{
			qDebug() << "Bad query: select Consumers.Id From Consumers where (Consumers.Log_in = '"
						+ userLogin +"')";
			return;
		}
		query.next();
		Id_user = query.value(0).toInt();
		//-------------------
		if(!query.exec("select Phone.Id From Phone where (Phone.Name = '"
					   + ui->comboBox_ChooseModel->currentText() + "')"))
		{
			qDebug() << "Bad query: select Phone.Id From Phone where (Phone.Name = '"
						+ ui->comboBox_ChooseModel->currentText() + "')" ;
			return;
		}
		query.next();
		Id_phone = query.value(0).toInt();
		qDebug() << query.value(0).toInt();
		
		//-------------------
		if(!query.exec("select count(*) From Apps"))
		{
			qDebug() << "Bad query: select count(*) From Apps";
			return;
		}
		if(query.value(0).toInt() != 0)
		{
			if(query.exec("select Apps.Id from Apps"))
			{
				qDebug() << "Bad query: select Apps.Id from Apps";
				return;
			}
			while(query.next())
				Id_app = query.value(0).toInt();
			Id_app++;
		}
		qDebug() << query.value(0).toInt();
		//-------------------
		str = "select Warehouse.PriceForOne From Warehouse where (";
		str += QString::number(Id_phone) + "=Warehouse.Id_Phone)";
		if(!query.exec(str))
		{
			qDebug() << "Bad query: "+str;
			return;
		}
		query.next();
		Price = query.value(0).toInt();
		
		str = "INSERT INTO Apps VALUES (";
		str += QString::number(Id_app) + ", ";
		str += QString::number(Id_user) + ", ";
		str += QString::number(Id_phone) + ", ";
		str += ui->comboBox_countModels->currentText() + ", ";
		str += QString::number(
				ui->comboBox_countModels->currentText().toInt()*Price);
		str += ", 0)";
		
		qDebug() << str;
		
		if(!query.exec(str))
		{
			qDebug() << "Bad query: " + str;
		}
		
		
	}
}

void Consumer_Dialog::on_pushButton_Cancel_clicked()
{
    this->hide();
}


void Consumer_Dialog::resizeEvent(QResizeEvent *)
{
	ui->tableWidget_Phones->resize(this->width()-319,this->height()-19);
}


void Consumer_Dialog::setDB(QSqlDatabase *db)
{
	this->db = db;
}


void Consumer_Dialog::firstUpdateView()
{
	QSqlQuery query;
	
// Table
	QString str = "Select VendorPhone.Name,Phone.Name, ";
	str+="Phone.DiagScreen,Phone.FlashMem_GB, ";
	str+="Phone.SDcard,Phone.Camera,Phone.FrontCamera, ";
	str+="VendorSoC.Name,SoC.Name,SoC.Freq, Warehouse.Price, Warehouse.Count";
	str+="FROM Phone,VendorPhone,VendorSoC,SoC,Warehouse ";
	str+="Where (Phone.Id_Vendor=VendorPhone.Id AND ";
	str+="Phone.Id_SoC=SoC.Id AND ";
	str+="SoC.IdVendor=VendorSoC.Id AND Phone.Id=Warehouse.Id_Phone)";
	
	
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		int i=0;
		while(query.next())
		{
			ui->tableWidget_Phones->setRowCount(i+1);
			for(int j=0; j<query.record().count(); j++)
			{
				ui->tableWidget_Phones->setItem(i,j,
						new QTableWidgetItem(query.value(j).toString(),1));
			}
			i++;
		}
	}
	
// Comboboxes
	//VendorPhone
	str = "SELECT VendorPhone.Name FROM VendorPhone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_VendorPhone->addItem("No chosen");
		while(query.next())
			ui->comboBox_VendorPhone->addItem(
						query.value(0).toString());
	}
	//VendorSoC
	str = "SELECT VendorSoC.Name FROM VendorSoC";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_VendorSoC->addItem("No chosen");
		while(query.next())
			ui->comboBox_VendorSoC->addItem(
						query.value(0).toString());
	}
	//Frequency 1,2
	str = "SELECT DISTINCT SoC.Freq FROM SoC";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		while(query.next())
		{
			ui->comboBox_Freq1->addItem(
						query.value(0).toString());
			ui->comboBox_Freq2->addItem(
						query.value(0).toString());
		}
		ui->comboBox_Freq2->setCurrentIndex(
					ui->comboBox_Freq2->count()-1);
	}
	//Diagonal
	str = "SELECT DISTINCT Phone.DiagScreen FROM Phone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		while(query.next())
		{
			ui->comboBox_Diag1->addItem(
					query.value(0).toString());
			ui->comboBox_Diag2->addItem(
					query.value(0).toString());
		}
		ui->comboBox_Diag2->setCurrentIndex(
					ui->comboBox_Diag2->count()-1);
	}
	//RAM
	str = "SELECT DISTINCT SoC.RAM FROM SoC";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_RAM->addItem("No chosen");
		while(query.next())
		{
			ui->comboBox_RAM->addItem(
					query.value(0).toString());
		}
	}
	//Back Camera, MPix
	str = "SELECT DISTINCT Phone.Camera FROM Phone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_BackCam->addItem("No chosen");
		while(query.next())
		{
			ui->comboBox_BackCam->addItem(
					query.value(0).toString());
		}
	}
	//Front Camera
	str = "SELECT DISTINCT Phone.FrontCamera FROM Phone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_FrontCam->addItem("No chosen");
		while(query.next())
		{
			ui->comboBox_FrontCam->addItem(
					query.value(0).toString());
		}
	}
	//SD card
	str = "SELECT DISTINCT Phone.SDcard FROM Phone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_SDcard->addItem("No chosen");
		while(query.next())
		{
			ui->comboBox_SDcard->addItem(
					query.value(0).toString());
		}
	}
	//Flash memory
	str = "SELECT DISTINCT Phone.FlashMem_GB FROM Phone";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
	}
	else
	{
		ui->comboBox_FlashMem->addItem("No chosen");
		while(query.next())
		{
			ui->comboBox_FlashMem->addItem(
					query.value(0).toString());
		}
	}
}

void Consumer_Dialog::updateView()
{
	QSqlQuery query;
	QString str,
	str_forTable = "Select VendorPhone.Name,Phone.Name, ";
	str_forTable+="Phone.DiagScreen,Phone.FlashMem_GB, ";
	str_forTable+="Phone.SDcard,Phone.Camera,Phone.FrontCamera, ";
	str_forTable+="VendorSoC.Name,SoC.Name,SoC.Freq, Warehouse.PriceForOne, Warehouse.Count ";
	str_forTable+="FROM Phone,VendorPhone,VendorSoC,SoC,Warehouse ";
	str_forTable+="Where (Phone.Id_Vendor=VendorPhone.Id AND ";
	str_forTable+="Phone.Id_SoC=SoC.Id AND ";
	str_forTable+="SoC.IdVendor=VendorSoC.Id AND Phone.Id=Warehouse.Id_Phone ";
	
	QString str_forChoose = "Select Phone.Name ";
	str_forChoose+="FROM Phone,VendorPhone,VendorSoC,SoC,Warehouse ";
	str_forChoose+="Where (Phone.Id_Vendor=VendorPhone.Id AND ";
	str_forChoose+="Phone.Id_SoC=SoC.Id AND ";
	str_forChoose+="SoC.IdVendor=VendorSoC.Id AND Phone.Id=Warehouse.Id_Phone ";
	str_forChoose+="AND Warehouse.Count>0 ";
	
	
	if(ui->comboBox_VendorPhone->currentIndex() != 0 &&
			ui->comboBox_VendorPhone->currentText() !="")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		
		str = "'" + ui->comboBox_VendorPhone->currentText()
				+ "'=VendorPhone.Name ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	
	if(ui->comboBox_VendorSoC->currentIndex() != 0 &&
			ui->comboBox_VendorSoC->currentText() != "")
	{
		str_forChoose += "AND ";
		str_forTable += "AND ";
		
		str = "'" + ui->comboBox_VendorSoC->currentText()
				+ "'=VendorSoC.Name ";

		str_forChoose += str;
		str_forTable += str;
	}
	
	// For Freq and Diag
	if(ui->comboBox_Freq1->currentText() != "" &&
			ui->comboBox_Freq2->currentText() != "" &&
			ui->comboBox_Diag1->currentText() != "" &&
			ui->comboBox_Diag2->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
	
		str = ui->comboBox_Freq1->currentText()+"<=SoC.Freq AND "+
		ui->comboBox_Freq2->currentText()+">=SoC.Freq AND "+
		ui->comboBox_Diag1->currentText()+"<=Phone.DiagScreen AND " +
		ui->comboBox_Diag2->currentText()+">=Phone.DiagScreen ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	
	if(ui->comboBox_RAM->currentIndex() != 0 &&
			ui->comboBox_RAM->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		
		str = "SoC.RAM=" + ui->comboBox_RAM->currentText()
				+ " ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	if(ui->comboBox_FlashMem->currentIndex() != 0 &&
			ui->comboBox_FlashMem->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		
		str = "Phone.FlashMem_GB="
				+ ui->comboBox_FlashMem->currentText()
				+ " ";

		str_forChoose += str;
		str_forTable += str;
	}
	if(ui->comboBox_SDcard->currentIndex() != 0 &&
			ui->comboBox_SDcard->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		str = "Phone.SDcard='"
				+ ui->comboBox_SDcard->currentText()
				+ "' ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	if(ui->comboBox_FrontCam->currentIndex() != 0 &&
			ui->comboBox_FrontCam->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		str = "Phone.FrontCamera='"
				+ ui->comboBox_FrontCam->currentText()
				+ "' ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	if(ui->comboBox_BackCam->currentIndex() != 0 &&
			ui->comboBox_BackCam->currentText() != "")
	{
		str_forChoose +="AND ";
		str_forTable +="AND ";
		str = "Phone.Camera="
				+ ui->comboBox_BackCam->currentText()
				+ " ";
		
		str_forChoose += str;
		str_forTable += str;
	}
	
	if(ui->comboBox_Status->currentText() != "All" )
	{
		str_forTable +="AND ";
		str_forChoose +="AND ";
		str = "Warehouse.Count";
		
		if(ui->comboBox_Status->currentText() == "Exist" )
			str += ">0 ";
		else
			str += "=0 ";

		str_forTable += str;
		str_forChoose += str;
	}
	
	
	str_forChoose += ")";
	str_forTable += ")";
	
	qDebug() << str_forTable;
	
	if(!query.exec(str_forChoose))
	{
		qDebug() << "bad query str_forChoose";
	}
	else
	{
		while(ui->comboBox_ChooseModel->count()>1)
			ui->comboBox_ChooseModel->removeItem(1);
		
		while(query.next())
			ui->comboBox_ChooseModel->addItem(
						query.value(0).toString());
	}
	
	if(!query.exec(str_forTable))
	{
		qDebug() << "bad query str_forTable";
	}
	else
	{
		int i=0;
		while(query.next())
		{
			ui->tableWidget_Phones->setRowCount(i+1);
			for(int j=0; j<query.record().count(); j++)
			{
				ui->tableWidget_Phones->setItem(i,j,
						new QTableWidgetItem(query.value(j).toString(),1));
			}// от new нужно избавиться.
			i++;
		}
		if(!i) ui->tableWidget_Phones->setRowCount(i);
	}
	
}

void Consumer_Dialog::on_comboBox_VendorPhone_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_VendorSoC_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_Freq1_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_Freq2_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_Diag1_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_Diag2_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_RAM_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_FlashMem_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_SDcard_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_FrontCam_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::on_comboBox_BackCam_currentTextChanged(const QString &)
{
	this->updateView();
}

void Consumer_Dialog::setUser(QString login)
{
	this->userLogin = login;
}

void Consumer_Dialog::on_comboBox_ChooseModel_currentTextChanged(const QString &phone)
{
	if(phone!="No chosen")
	{
		ui->comboBox_countModels->setDisabled(0);
		while(ui->comboBox_countModels->count()!=0)
			ui->comboBox_countModels->removeItem(0);
		
		QSqlQuery query;
		int count;
		QString str = "SELECT Warehouse.Count FROM Warehouse, Phone Where";
		str += "(Phone.Id = Warehouse.Id_Phone and Phone.Name = '" + phone + "')";
		
		if(!query.exec(str))
		{
			qDebug() << "Bad query: "<< str;
			return;
		}
		query.next();
		count = query.value(0).toInt();
		
		for(int i=1; i<=count; i++)
			ui->comboBox_countModels->addItem(QString::number(i));
	}
	else
	{
		ui->comboBox_countModels->setDisabled(1);
		while(ui->comboBox_countModels->count()!=0)
			ui->comboBox_countModels->removeItem(0);
		ui->comboBox_countModels->addItem("No chosen");
	}
}

void Consumer_Dialog::on_comboBox_Status_currentTextChanged(const QString &)
{
	this->updateView();
}
