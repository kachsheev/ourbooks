#-------------------------------------------------
#
# Project created by QtCreator 2013-11-06T12:10:34
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    admin_dialog.cpp \
    consumer_dialog.cpp \
    register_dialog.cpp \
    wait_dialog.cpp \
    disp_dialog.cpp \
    seeapp_dialog.cpp

HEADERS  += mainwindow.h \
    admin_dialog.h \
    consumer_dialog.h \
    register_dialog.h \
    wait_dialog.h \
    disp_dialog.h \
    seeapp_dialog.h

FORMS    += mainwindow.ui \
    admin_dialog.ui \
    consumer_dialog.ui \
    register_dialog.ui \
    wait_dialog.ui \
    disp_dialog.ui \
    seeapp_dialog.ui
