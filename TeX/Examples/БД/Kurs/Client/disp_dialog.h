#ifndef DISP_DIALOG_H
#define DISP_DIALOG_H

#include "seeapp_dialog.h"

#include <QDialog>
#include <QSql>
#include <QSqlDatabase>

namespace Ui {
class Disp_Dialog;
}

class Disp_Dialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit Disp_Dialog(QWidget *parent = 0);
	~Disp_Dialog();
	
	void setDB(QSqlDatabase *db);
	
private slots:
	void on_pushButton_RefreshTable_clicked();
	
	void on_pushButton_SeeApp_clicked();
	
private:
	Ui::Disp_Dialog *ui;
	
	QSqlDatabase *db;
	SeeApp_Dialog *seeAapp;
};

#endif // DISP_DIALOG_H
