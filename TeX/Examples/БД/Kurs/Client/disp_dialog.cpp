#include "disp_dialog.h"
#include "ui_disp_dialog.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>
#include <QColor>

Disp_Dialog::Disp_Dialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Disp_Dialog)
{
	ui->setupUi(this);
	
	QStringList str_list;
	
	str_list << "№" << "User" << "Vendor Phone" 
			 << "Name Phone" << "Count"
			 << "Total Price";
	ui->tableWidget->setColumnCount(str_list.size());
	ui->tableWidget->setHorizontalHeaderLabels(str_list);
	
	seeAapp = new SeeApp_Dialog(this);
}

Disp_Dialog::~Disp_Dialog()
{
	delete ui;
}

void Disp_Dialog::setDB(QSqlDatabase *db)
{
	this->db = db;
}


void Disp_Dialog::on_pushButton_RefreshTable_clicked()
{
	QColor red(255,128,128,255), yellow(255,255,0,255), green(0,255,0,255);
	QSqlQuery query;
	QString str = "select Apps.Id,Consumers.Log_in,VendorPhone.Name,Phone.Name,Apps.Count,Apps.TotalPrice,Apps.Status ";
			str += "from Apps,Phone,VendorPhone,Consumers where";
			str += "(Apps.Id_client=Consumers.Id AND ";
			str += "Phone.Id=Apps.Id_phone AND ";
			str += "Phone.Id_vendor=VendorPhone.Id)";
			
	if(!query.exec(str))
	{
		qDebug() << "Bad query: "+str;
		return;
	}
	int j = 0;
	while(query.next())
	{
		ui->tableWidget->setRowCount(j+1);
		for(int i=0; i<query.record().count()-1; i++)
		{
			ui->tableWidget->setItem(j,i,
					new QTableWidgetItem(query.value(i).toString(),1));
		}
		switch(query.value(query.record().count()-1).toInt())
		{
		case 0:
			for(int i=0; i<query.record().count()-1; i++)
				ui->tableWidget->item(j,i)->setBackgroundColor(red);
			break;
		case 1:
			break;
		case 2:
			break;
		}
		
		j++;
	}
	
}

void Disp_Dialog::on_pushButton_SeeApp_clicked()
{
	seeAapp->show();
}
