#include "register_dialog.h"
#include "ui_register_dialog.h"

#include <QCryptographicHash>
#include <QSqlQuery>
#include <QDebug>

Register_Dialog::Register_Dialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Register_Dialog)
{
	ui->setupUi(this);
	this->parent = parent;
	
	ui->lineEdit_Passwd1->setEchoMode(QLineEdit::Password);
	ui->lineEdit_Passwd2->setEchoMode(QLineEdit::Password);

	this->setFixedSize(this->width(),this->height());
}

Register_Dialog::~Register_Dialog()
{
	delete ui;
}


void Register_Dialog::on_pushButton_OK_clicked()
{
	if(validateNewUser())
	{
		addNewUser();
	}
}

void Register_Dialog::on_pushButton_Cancel_clicked()
{
    this->hide();
	parent->show();
}
void Register_Dialog::setDB(QSqlDatabase *db)
{
	this->db = db;
}

int Register_Dialog::validateNewUser()
{
	if(ui->lineEdit_Email1->text()==""||
			ui->lineEdit_Email2->text()=="")
	{
		qDebug() << "Enter your email!";
		return 0;
	}
	
	if(ui->lineEdit_Passwd1->text()==""||
			ui->lineEdit_Passwd2->text()=="")
	{
		qDebug() << "Enter password!";
		return 0;
	}
	
	
	if(ui->lineEdit_Email1->text()!=
			ui->lineEdit_Email2->text())
	{
		qDebug() << "Uncorrect Email";
		return 0;
	}
	
	if(ui->lineEdit_Passwd1->text()!=
			ui->lineEdit_Passwd2->text())
	{
		qDebug() << "Uncorrect Email";
		return 0;
	}
	
	QSqlQuery query(*db);
		
	QString str = "SELECT COUNT(*) FROM Consumers WHERE (";
	str += "'"+ui->lineEdit_Login->text()+"'=Log_in)";
	if(!query.exec(str))
	{
		qDebug() << "Bad query";
		return 0;
	}
	query.next();
	if(0<query.value(0).toInt())
	{
		qDebug() << "This user is exist"; 
		return 0;
	}
	
	return 1;
}

void Register_Dialog::addNewUser()
{
	QSqlQuery query(*db);
	
	int ID,permission = 1;
	
	if(query.exec("SELECT COUNT(*) FROM Consumers"))
	{
		query.next();
		ID = query.value(0).toInt() + 1;
		qDebug() << ID;
	}
	else
	{
		qDebug() << "Can not run query";
		return;
	}
	
	QString login(ui->lineEdit_Login->text()),
			password(ui->lineEdit_Passwd1->text()),
			email(ui->lineEdit_Email1->text()),
			name(ui->lineEdit_Name->text()),
			lastName(ui->lineEdit_LastName->text()),
			cellPhone(ui->lineEdit_CellPhone->text());
	
//	QByteArray login_hash = QCryptographicHash::hash
//			(login.toUtf8(), QCryptographicHash::Md5); login = "";
	QByteArray password_hash = QCryptographicHash::hash
			(password.toUtf8(), QCryptographicHash::Md5); password = "";
	
//	login.append(login_hash);
	password.append(password_hash);
	
	QString str = "INSERT INTO Consumers VALUES (";
	str += QString::number(ID) + ", '";
	str += name + "', '";
	str += lastName + "', '";
	str += cellPhone + "', '";
	str += login + "', '";
	str += password + "', '";
	str += email + "',";
	str += QString::number(permission) + ")";
	qDebug() << str;
	if(!query.exec(str))
	{
		qDebug() << "Bad";
	}
	else
	{
		qDebug() << "ALL RIGHT!";
		
		ui->lineEdit_Email1->setText("");
		ui->lineEdit_Email2->setText("");
		ui->lineEdit_LastName->setText("");
		ui->lineEdit_Login->setText("");
		ui->lineEdit_Name->setText("");
		ui->lineEdit_Passwd1->setText("");
		ui->lineEdit_Passwd2->setText("");
		
		this->hide();
	}
}
