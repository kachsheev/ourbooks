/********************************************************************************
** Form generated from reading UI file 'disp_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DISP_DIALOG_H
#define UI_DISP_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Disp_Dialog
{
public:
    QTableWidget *tableWidget;
    QPushButton *pushButton_SeeApp;
    QComboBox *comboBox_IdApp;
    QPushButton *pushButton_RefreshTable;

    void setupUi(QDialog *Disp_Dialog)
    {
        if (Disp_Dialog->objectName().isEmpty())
            Disp_Dialog->setObjectName(QStringLiteral("Disp_Dialog"));
        Disp_Dialog->resize(540, 490);
        tableWidget = new QTableWidget(Disp_Dialog);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 522, 431));
        pushButton_SeeApp = new QPushButton(Disp_Dialog);
        pushButton_SeeApp->setObjectName(QStringLiteral("pushButton_SeeApp"));
        pushButton_SeeApp->setGeometry(QRect(10, 450, 101, 31));
        comboBox_IdApp = new QComboBox(Disp_Dialog);
        comboBox_IdApp->setObjectName(QStringLiteral("comboBox_IdApp"));
        comboBox_IdApp->setGeometry(QRect(120, 450, 61, 31));
        pushButton_RefreshTable = new QPushButton(Disp_Dialog);
        pushButton_RefreshTable->setObjectName(QStringLiteral("pushButton_RefreshTable"));
        pushButton_RefreshTable->setGeometry(QRect(430, 450, 101, 31));

        retranslateUi(Disp_Dialog);

        QMetaObject::connectSlotsByName(Disp_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Disp_Dialog)
    {
        Disp_Dialog->setWindowTitle(QApplication::translate("Disp_Dialog", "Dialog", 0));
        pushButton_SeeApp->setText(QApplication::translate("Disp_Dialog", "See App", 0));
        pushButton_RefreshTable->setText(QApplication::translate("Disp_Dialog", "Refresh Table", 0));
    } // retranslateUi

};

namespace Ui {
    class Disp_Dialog: public Ui_Disp_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DISP_DIALOG_H
