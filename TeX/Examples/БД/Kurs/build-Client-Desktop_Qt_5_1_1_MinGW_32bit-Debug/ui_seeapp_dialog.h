/********************************************************************************
** Form generated from reading UI file 'seeapp_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEEAPP_DIALOG_H
#define UI_SEEAPP_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SeeApp_Dialog
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_Login;
    QLabel *label_userName;
    QLabel *label_userLastName;
    QLabel *label_userCellPhone;
    QLabel *label_chosenPhone;
    QLabel *label_countPhones;
    QLabel *label_TotalPrice;
    QLabel *label_8;
    QComboBox *comboBox;
    QPushButton *pushButton_OK;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *SeeApp_Dialog)
    {
        if (SeeApp_Dialog->objectName().isEmpty())
            SeeApp_Dialog->setObjectName(QStringLiteral("SeeApp_Dialog"));
        SeeApp_Dialog->resize(270, 290);
        label = new QLabel(SeeApp_Dialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 71, 20));
        label_2 = new QLabel(SeeApp_Dialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 40, 71, 20));
        label_3 = new QLabel(SeeApp_Dialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 70, 71, 20));
        label_4 = new QLabel(SeeApp_Dialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 100, 71, 20));
        label_5 = new QLabel(SeeApp_Dialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 130, 71, 20));
        label_6 = new QLabel(SeeApp_Dialog);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(20, 160, 71, 20));
        label_7 = new QLabel(SeeApp_Dialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(20, 190, 71, 20));
        label_Login = new QLabel(SeeApp_Dialog);
        label_Login->setObjectName(QStringLiteral("label_Login"));
        label_Login->setGeometry(QRect(110, 10, 151, 20));
        label_userName = new QLabel(SeeApp_Dialog);
        label_userName->setObjectName(QStringLiteral("label_userName"));
        label_userName->setGeometry(QRect(110, 40, 151, 20));
        label_userLastName = new QLabel(SeeApp_Dialog);
        label_userLastName->setObjectName(QStringLiteral("label_userLastName"));
        label_userLastName->setGeometry(QRect(110, 70, 151, 20));
        label_userCellPhone = new QLabel(SeeApp_Dialog);
        label_userCellPhone->setObjectName(QStringLiteral("label_userCellPhone"));
        label_userCellPhone->setGeometry(QRect(110, 100, 151, 20));
        label_chosenPhone = new QLabel(SeeApp_Dialog);
        label_chosenPhone->setObjectName(QStringLiteral("label_chosenPhone"));
        label_chosenPhone->setGeometry(QRect(110, 130, 151, 20));
        label_countPhones = new QLabel(SeeApp_Dialog);
        label_countPhones->setObjectName(QStringLiteral("label_countPhones"));
        label_countPhones->setGeometry(QRect(110, 160, 151, 20));
        label_TotalPrice = new QLabel(SeeApp_Dialog);
        label_TotalPrice->setObjectName(QStringLiteral("label_TotalPrice"));
        label_TotalPrice->setGeometry(QRect(110, 190, 151, 20));
        label_8 = new QLabel(SeeApp_Dialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 220, 71, 20));
        comboBox = new QComboBox(SeeApp_Dialog);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(110, 220, 151, 22));
        pushButton_OK = new QPushButton(SeeApp_Dialog);
        pushButton_OK->setObjectName(QStringLiteral("pushButton_OK"));
        pushButton_OK->setGeometry(QRect(10, 250, 75, 31));
        pushButton_Cancel = new QPushButton(SeeApp_Dialog);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setGeometry(QRect(190, 250, 75, 31));

        retranslateUi(SeeApp_Dialog);

        QMetaObject::connectSlotsByName(SeeApp_Dialog);
    } // setupUi

    void retranslateUi(QDialog *SeeApp_Dialog)
    {
        SeeApp_Dialog->setWindowTitle(QApplication::translate("SeeApp_Dialog", "Dialog", 0));
        label->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Login</p></body></html>", 0));
        label_2->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Name</p></body></html>", 0));
        label_3->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Last name</p></body></html>", 0));
        label_4->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Cell phone</p></body></html>", 0));
        label_5->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Model</p></body></html>", 0));
        label_6->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Count</p></body></html>", 0));
        label_7->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Total price</p></body></html>", 0));
        label_Login->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_userName->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_userLastName->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_userCellPhone->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_chosenPhone->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_countPhones->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_TotalPrice->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p>TextLabel</p></body></html>", 0));
        label_8->setText(QApplication::translate("SeeApp_Dialog", "<html><head/><body><p align=\"right\">Change status</p></body></html>", 0));
        pushButton_OK->setText(QApplication::translate("SeeApp_Dialog", "OK", 0));
        pushButton_Cancel->setText(QApplication::translate("SeeApp_Dialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class SeeApp_Dialog: public Ui_SeeApp_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEEAPP_DIALOG_H
