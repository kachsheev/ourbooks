/****************************************************************************
** Meta object code from reading C++ file 'consumer_dialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Client/consumer_dialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'consumer_dialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Consumer_Dialog_t {
    QByteArrayData data[18];
    char stringdata[586];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Consumer_Dialog_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Consumer_Dialog_t qt_meta_stringdata_Consumer_Dialog = {
    {
QT_MOC_LITERAL(0, 0, 15),
QT_MOC_LITERAL(1, 16, 27),
QT_MOC_LITERAL(2, 44, 0),
QT_MOC_LITERAL(3, 45, 28),
QT_MOC_LITERAL(4, 74, 42),
QT_MOC_LITERAL(5, 117, 40),
QT_MOC_LITERAL(6, 158, 36),
QT_MOC_LITERAL(7, 195, 36),
QT_MOC_LITERAL(8, 232, 36),
QT_MOC_LITERAL(9, 269, 36),
QT_MOC_LITERAL(10, 306, 34),
QT_MOC_LITERAL(11, 341, 39),
QT_MOC_LITERAL(12, 381, 37),
QT_MOC_LITERAL(13, 419, 39),
QT_MOC_LITERAL(14, 459, 38),
QT_MOC_LITERAL(15, 498, 37),
QT_MOC_LITERAL(16, 536, 42),
QT_MOC_LITERAL(17, 579, 5)
    },
    "Consumer_Dialog\0on_pushButton_Order_clicked\0"
    "\0on_pushButton_Cancel_clicked\0"
    "on_comboBox_VendorPhone_currentTextChanged\0"
    "on_comboBox_VendorSoC_currentTextChanged\0"
    "on_comboBox_Freq1_currentTextChanged\0"
    "on_comboBox_Freq2_currentTextChanged\0"
    "on_comboBox_Diag1_currentTextChanged\0"
    "on_comboBox_Diag2_currentTextChanged\0"
    "on_comboBox_RAM_currentTextChanged\0"
    "on_comboBox_FlashMem_currentTextChanged\0"
    "on_comboBox_SDcard_currentTextChanged\0"
    "on_comboBox_FrontCam_currentTextChanged\0"
    "on_comboBox_BackCam_currentTextChanged\0"
    "on_comboBox_Status_currentTextChanged\0"
    "on_comboBox_ChooseModel_currentTextChanged\0"
    "phone\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Consumer_Dialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x08,
       3,    0,   90,    2, 0x08,
       4,    1,   91,    2, 0x08,
       5,    1,   94,    2, 0x08,
       6,    1,   97,    2, 0x08,
       7,    1,  100,    2, 0x08,
       8,    1,  103,    2, 0x08,
       9,    1,  106,    2, 0x08,
      10,    1,  109,    2, 0x08,
      11,    1,  112,    2, 0x08,
      12,    1,  115,    2, 0x08,
      13,    1,  118,    2, 0x08,
      14,    1,  121,    2, 0x08,
      15,    1,  124,    2, 0x08,
      16,    1,  127,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,   17,

       0        // eod
};

void Consumer_Dialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Consumer_Dialog *_t = static_cast<Consumer_Dialog *>(_o);
        switch (_id) {
        case 0: _t->on_pushButton_Order_clicked(); break;
        case 1: _t->on_pushButton_Cancel_clicked(); break;
        case 2: _t->on_comboBox_VendorPhone_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_comboBox_VendorSoC_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_comboBox_Freq1_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_comboBox_Freq2_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_comboBox_Diag1_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_comboBox_Diag2_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->on_comboBox_RAM_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->on_comboBox_FlashMem_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->on_comboBox_SDcard_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->on_comboBox_FrontCam_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->on_comboBox_BackCam_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->on_comboBox_Status_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->on_comboBox_ChooseModel_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject Consumer_Dialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Consumer_Dialog.data,
      qt_meta_data_Consumer_Dialog,  qt_static_metacall, 0, 0}
};


const QMetaObject *Consumer_Dialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Consumer_Dialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Consumer_Dialog.stringdata))
        return static_cast<void*>(const_cast< Consumer_Dialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int Consumer_Dialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
