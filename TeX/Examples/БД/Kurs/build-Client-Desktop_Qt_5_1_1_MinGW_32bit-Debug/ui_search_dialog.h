/********************************************************************************
** Form generated from reading UI file 'search_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCH_DIALOG_H
#define UI_SEARCH_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Search_Dialog
{
public:
    QPushButton *pushButton_OK;
    QTableWidget *tableWidget;

    void setupUi(QDialog *Search_Dialog)
    {
        if (Search_Dialog->objectName().isEmpty())
            Search_Dialog->setObjectName(QStringLiteral("Search_Dialog"));
        Search_Dialog->resize(640, 480);
        pushButton_OK = new QPushButton(Search_Dialog);
        pushButton_OK->setObjectName(QStringLiteral("pushButton_OK"));
        pushButton_OK->setGeometry(QRect(570, 439, 61, 31));
        tableWidget = new QTableWidget(Search_Dialog);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 391, 461));

        retranslateUi(Search_Dialog);

        QMetaObject::connectSlotsByName(Search_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Search_Dialog)
    {
        Search_Dialog->setWindowTitle(QApplication::translate("Search_Dialog", "Dialog", 0));
        pushButton_OK->setText(QApplication::translate("Search_Dialog", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class Search_Dialog: public Ui_Search_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCH_DIALOG_H
