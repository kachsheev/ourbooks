/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit_Login;
    QLineEdit *lineEdit_Passwd;
    QPushButton *pushButton_Register;
    QPushButton *pushButton_Connect;
    QLabel *label_Passwd;
    QLabel *label_Login;
    QPushButton *pushButton_Exit;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(260, 120);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit_Login = new QLineEdit(centralWidget);
        lineEdit_Login->setObjectName(QStringLiteral("lineEdit_Login"));
        lineEdit_Login->setGeometry(QRect(70, 10, 113, 21));
        lineEdit_Passwd = new QLineEdit(centralWidget);
        lineEdit_Passwd->setObjectName(QStringLiteral("lineEdit_Passwd"));
        lineEdit_Passwd->setGeometry(QRect(70, 40, 113, 21));
        pushButton_Register = new QPushButton(centralWidget);
        pushButton_Register->setObjectName(QStringLiteral("pushButton_Register"));
        pushButton_Register->setGeometry(QRect(30, 80, 75, 31));
        pushButton_Connect = new QPushButton(centralWidget);
        pushButton_Connect->setObjectName(QStringLiteral("pushButton_Connect"));
        pushButton_Connect->setGeometry(QRect(190, 20, 61, 31));
        label_Passwd = new QLabel(centralWidget);
        label_Passwd->setObjectName(QStringLiteral("label_Passwd"));
        label_Passwd->setGeometry(QRect(10, 40, 51, 21));
        label_Login = new QLabel(centralWidget);
        label_Login->setObjectName(QStringLiteral("label_Login"));
        label_Login->setGeometry(QRect(10, 10, 51, 21));
        pushButton_Exit = new QPushButton(centralWidget);
        pushButton_Exit->setObjectName(QStringLiteral("pushButton_Exit"));
        pushButton_Exit->setGeometry(QRect(160, 80, 71, 31));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MS SQL Client", 0));
        pushButton_Register->setText(QApplication::translate("MainWindow", "Register", 0));
        pushButton_Connect->setText(QApplication::translate("MainWindow", "Enter", 0));
        label_Passwd->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">Password</p></body></html>", 0));
        label_Login->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">Login</p></body></html>", 0));
        pushButton_Exit->setText(QApplication::translate("MainWindow", "Exit", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
